import org.gradle.api.tasks.bundling.Jar
import org.jetbrains.intellij.tasks.PatchPluginXmlTask
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.3.70"
    kotlin("plugin.serialization") version "1.3.70"
    id("org.jetbrains.intellij") version "0.4.16"
    id("com.moowork.node") version "1.3.1"
}

intellij {
    version = "201.7223.91"
    updateSinceUntilBuild = false
    setPlugins("java")
}

group = "io.github.mreditor.codesimilarity"
version = "1.0.0"

repositories {
    jcenter()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("org.jetbrains.kotlinx", "kotlinx-serialization-runtime", "0.20.0")
}

tasks.withType<Jar> {
    into("static") {
        val webapp = "src/main/webapp"
        val nodePath = "$webapp/node_modules"
        from("$webapp/static")
        File("$rootDir/$nodePath").list()?.forEach {
            from("$nodePath/$it/dist")
        }
    }
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

tasks.withType<PatchPluginXmlTask> {
    setSinceBuild(intellij.version)
}
