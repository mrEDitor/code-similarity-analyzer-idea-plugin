package io.github.mreditor.codesimilarity.plugin.idea.models

open class Datum(
        val id: Int,
        val name: String,
        val filename: String,
        val line: Int,
        val column: Int,
        val code: String,
        val owner: Any,
        val source: Any
) {
    val inputs: DoubleLinkedSet<Node, Datum> = DoubleLinkedSet(this, Node::inputs)
    val outputs: DoubleLinkedSet<Node, Datum> = DoubleLinkedSet(this, Node::outputs)
}

