package io.github.mreditor.codesimilarity.plugin.idea.psi

import com.intellij.psi.*
import io.github.mreditor.codesimilarity.plugin.idea.models.Node
import io.github.mreditor.codesimilarity.plugin.idea.models.*
import io.github.mreditor.codesimilarity.plugin.idea.http.NodeCache

class PsiNodeFactory(private val cache: NodeCache) {
    private val data = PsiDataContext()

    fun createNode(element: PsiElement, parent: Node? = null): Node {
        cache.get(element.hashCode())?.let { return it }

        val node = ExpressionNode(
                shrinkCode(element.text),
                element.containingFile.name,
                element.line,
                element.column,
                parent,
                element,
                isCommutative = element is PsiCodeBlock
        )
        cache.add(node)
        when (element) {
            is PsiAssignmentExpression -> node.addChildren(element)
            is PsiMethodCallExpression -> node.addChildren(element)
            is PsiUnaryExpression -> node.addChildren(element)
            is PsiLocalVariable -> node.addChildren(element)
            is PsiParameter -> node.addChildren(element)
            is PsiReferenceExpression -> node.addChildren(element)
            /// TODO more commutative operations
            else -> node.addChildren(element)
        }

        return node
    }

    private fun ExpressionNode.addChildren(element: PsiAssignmentExpression) {
        addChildren(element as PsiElement)
        outputs += children.first { it.source === element.lExpression }.result
        inputs += children.first { it.source === element.rExpression }.result
    }

    private fun ExpressionNode.addChildren(element: PsiMethodCallExpression) {
        addChildren(element as PsiElement)
        val impl = createNode(element.resolveMethod() ?: element.methodExpression)
        inputs += impl.inputs
        outputs += impl.outputs
    }

    private fun ExpressionNode.addChildren(element: PsiUnaryExpression) {
        addChildren(element as PsiElement)
        when (element.operationSign.text) {
            "++", "--" -> outputs += children.flatMap { it.result }
        }
    }

    private fun ExpressionNode.addChildren(element: PsiLocalVariable) {
        addChildren(element as PsiElement)
        outputs += data.create(element)
    }

    private fun ExpressionNode.addChildren(element: PsiParameter) {
        addChildren(element as PsiElement)
        outputs += data.create(element)
    }

    private fun ExpressionNode.addChildren(element: PsiReferenceExpression) {
        addChildren(element as PsiElement)
        result += data.get(element.references.mapNotNull { it.resolve() })
    }

    private fun ExpressionNode.addChildren(source: PsiElement) {
        children.addAll(
                source.children
                        .filterNot { it is PsiComment || it.text.isBlank() }
                        .mapNotNull { createNodeOrNull(it, this) }
        )
        inputs += children.flatMap { it.result }
        inputs += children.flatMap { it.inputs }.filter { it.owner != source }
        outputs += children.flatMap { it.outputs }.filter { it.owner != source }
    }

    private fun shrinkCode(text: String): String {
        val lines = text.lines()
        return lines.singleOrNull()
                ?: "${lines.first().trim()} ... ${lines.last().trim()}"
    }

    private fun createNodeOrNull(element: PsiElement, parent: Node? = null) =
            when (element) {
                is PsiJavaToken, is PsiKeyword -> null
                is PsiModifierList, is PsiTypeElement -> null
                else -> createNode(element, parent)
            }
}
