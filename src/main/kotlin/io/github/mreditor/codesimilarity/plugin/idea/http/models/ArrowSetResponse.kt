package io.github.mreditor.codesimilarity.plugin.idea.http.models

import kotlinx.serialization.Serializable

@Serializable
data class ArrowSetResponse(
        val from: ArrowResponse? = null,
        val to: ArrowResponse? = null
)
