package io.github.mreditor.codesimilarity.plugin.idea.http.models

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration

@Serializable
data class Response(
        val nodes: List<NodeResponse> = emptyList(),
        val data: List<DatumResponse> = emptyList(),
        val edges: List<EdgeResponse> = emptyList()
) {
    constructor(
            nodes: Sequence<NodeResponse>,
            data: Sequence<DatumResponse> = emptySequence(),
            edges: Sequence<EdgeResponse> = emptySequence()
    ) : this(
            nodes.toList(),
            data.toList(),
            edges.toList()
    )

    fun toJson() = Json(JsonConfiguration.Stable).toJson(serializer(), this)
}
