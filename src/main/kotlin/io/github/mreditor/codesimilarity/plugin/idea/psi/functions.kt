package io.github.mreditor.codesimilarity.plugin.idea.psi

import com.intellij.psi.PsiElement

val PsiElement.line
    get() = document.getLineNumber(textOffset) + 1

val PsiElement.column
    get() = textOffset - document.charsSequence.lastIndexOf('\n', textOffset)

val PsiElement.document
    get() = containingFile.viewProvider.document
            ?: throw UnsupportedOperationException()
