package io.github.mreditor.codesimilarity.plugin.idea.actions

import com.intellij.lang.java.JavaLanguage
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.CommonDataKeys
import com.intellij.psi.PsiElement
import io.github.mreditor.codesimilarity.plugin.idea.psi.PsiNodeFactory
import io.github.mreditor.codesimilarity.plugin.idea.http.NodeCache
import io.github.mreditor.codesimilarity.plugin.idea.http.models.*
import io.github.mreditor.codesimilarity.plugin.idea.ui.GraphWebRendererDialog

class ShowDataFlowGraphAction : AnAction() {

    override fun update(e: AnActionEvent) {
        val psi = e.getData(CommonDataKeys.PSI_ELEMENT)
        e.presentation.isEnabled = psi?.language == JavaLanguage.INSTANCE
    }

    override fun actionPerformed(e: AnActionEvent) {
        val psiElement = e.getData(CommonDataKeys.PSI_ELEMENT) as PsiElement
        val nodeCache = NodeCache()
        val root = PsiNodeFactory(nodeCache).createNode(psiElement)
        GraphWebRendererDialog(psiElement.project, "Data flow graph") { request ->
            val children = nodeCache.get(request.id.takeIf { it != 0 } ?: root.id)
                    ?.let { arrayOf(it, *it.children.toTypedArray()) }
                    ?: emptyArray()
            val nodes = children.map(NodeResponse.Companion::fromNode)
            val data = children
                    .flatMapTo(HashSet()) { it.inputs + it.outputs }
                    .map(DatumResponse.Factory::fromDatum)
            val nodeEdges = children.map(EdgeResponse.Companion::fromParent)
            val inputEdges = children.flatMap(EdgeResponse.Companion::fromInputs)
            val outputEdges = children.flatMap(EdgeResponse.Companion::fromOutputs)
            val resultEdges = children.flatMap(EdgeResponse.Companion::fromResult)
            return@GraphWebRendererDialog Response(
                    nodes = nodes,
                    data = data,
                    edges = nodeEdges + inputEdges + outputEdges + resultEdges
            )
        }
    }
}
