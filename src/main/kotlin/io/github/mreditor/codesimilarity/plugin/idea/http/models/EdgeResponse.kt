package io.github.mreditor.codesimilarity.plugin.idea.http.models

import io.github.mreditor.codesimilarity.plugin.idea.models.Node
import kotlinx.serialization.Serializable

@Serializable
data class EdgeResponse(
        val from: Int,
        val to: Int,
        val label: String? = null,
        val dashes: Boolean = false,
        val arrows: ArrowSetResponse? = null
) {
    val id = hashCode()

    companion object {
        private val arrowFrom = ArrowSetResponse(from = ArrowResponse())
        private val arrowTo = ArrowSetResponse(to = ArrowResponse())

        fun fromPair(pair: Pair<Node, Node>) =
                EdgeResponse(
                        pair.first.id,
                        pair.second.id,
                        arrows = arrowTo
                )

        fun fromReversedPair(pair: Pair<Node, Node>) =
                EdgeResponse(
                        pair.second.id,
                        pair.first.id,
                        dashes = true,
                        arrows = arrowFrom
                )

        fun fromParent(node: Node) =
                EdgeResponse(node.parent?.id ?: 0, node.id, dashes = true)

        fun fromInputs(node: Node) =
                node.inputs.map {
                    EdgeResponse(node.id, it.id, arrows = arrowFrom)
                }

        fun fromOutputs(node: Node) =
                node.outputs.map {
                    EdgeResponse(node.id, it.id, arrows = arrowTo)
                }

        fun fromResult(node: Node) =
                node.result.map {
                    EdgeResponse(node.id, it.id)
                }
    }
}