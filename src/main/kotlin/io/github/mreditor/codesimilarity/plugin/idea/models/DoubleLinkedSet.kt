package io.github.mreditor.codesimilarity.plugin.idea.models

class DoubleLinkedSet<TValue, THolder>(
        private val holder: THolder,
        private val callback: TValue.() -> DoubleLinkedSet<THolder, TValue>,
        private val underlying: MutableSet<TValue> = HashSet()
) : Set<TValue> by underlying {
    operator fun plusAssign(element: TValue) {
        if (underlying.add(element)) {
            callback(element).underlying.add(holder)
        }
    }

    operator fun plusAssign(element: Iterable<TValue>) =
            element.forEach(::plusAssign)
}
