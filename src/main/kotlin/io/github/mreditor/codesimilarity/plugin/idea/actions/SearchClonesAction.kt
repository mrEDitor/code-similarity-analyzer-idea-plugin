package io.github.mreditor.codesimilarity.plugin.idea.actions

import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.CommonDataKeys
import com.intellij.openapi.project.Project
import io.github.mreditor.codesimilarity.plugin.idea.ui.SearchClonesDialog

class SearchClonesAction : AnAction() {
    override fun actionPerformed(e: AnActionEvent) {
        val project = e.getData(CommonDataKeys.PROJECT) as Project
        SearchClonesDialog(project)
    }
}