package io.github.mreditor.codesimilarity.plugin.idea.http.models

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration

@Serializable
data class Request(val id: Int) {
    companion object Factory {
        fun fromJson(json: String) = Json(JsonConfiguration.Stable).parse(serializer(), json)
    }
}
