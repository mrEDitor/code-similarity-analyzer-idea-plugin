package io.github.mreditor.codesimilarity.plugin.idea.psi

import com.intellij.psi.PsiElement
import com.intellij.psi.PsiNamedElement
import io.github.mreditor.codesimilarity.plugin.idea.models.Datum

class PsiDataContext {
    private val data = mutableMapOf<PsiElement, Datum>()

    fun create(vararg references: PsiNamedElement) =
            create(references.toList())

    fun create(references: Iterable<PsiNamedElement>): Iterable<Datum> {
        references.firstOrNull(data::containsKey)?.let {
            throw UnsupportedOperationException("Variable $it already exists.")
        }

        return references.map {
            val datum = PsiDatum(it)
            data[it] = datum
            return@map datum
        }
    }

    fun get(references: Iterable<PsiElement>) =
            references.mapNotNull { data[it] }
}
