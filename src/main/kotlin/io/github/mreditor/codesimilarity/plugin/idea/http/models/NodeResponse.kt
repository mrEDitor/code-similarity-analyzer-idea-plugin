package io.github.mreditor.codesimilarity.plugin.idea.http.models

import com.intellij.psi.PsiMethod
import io.github.mreditor.codesimilarity.plugin.idea.models.Node
//import io.github.mreditor.codesimilarity.plugin.idea.dataflow.nodes.MethodNode
import kotlinx.serialization.Serializable

@Serializable
data class NodeResponse(
        val id: Int,
        val position: Int,
        val label: String,
        val title: String,
        val color: String,
        val parent: Int,
        val children: List<Int>
) {
    companion object {
        fun fromNode(node: Node) = NodeResponse(
                node.id,
                (node.line shl 20) or node.column,
                node.code,
                "${node.filename}:${node.line}:${node.column}<br>${node.source.javaClass}",
                node.color,
                node.parent?.id ?: 0,
                node.children.map { it.id }
        )

        private val Node.color
            get() = when {
                source is PsiMethod -> "#FF8782"
                children.isEmpty() -> "#C1FF90"
                else -> "#B2EBFF"
            }
    }
}

