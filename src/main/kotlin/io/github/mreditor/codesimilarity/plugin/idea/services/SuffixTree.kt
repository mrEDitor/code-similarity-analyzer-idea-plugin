package io.github.mreditor.codesimilarity.plugin.idea.services

import io.github.mreditor.codesimilarity.plugin.idea.models.Node
import io.github.mreditor.codesimilarity.plugin.idea.models.similarTo
import kotlin.math.min

class SuffixTree {
    private val rootNode = RootNode()
    private val _nodes
        get() = generateBreadthFirstSequence<SuffixNode>(rootNode) { it.children }

    val root = SuffixPointer(rootNode)

    val nodes: Sequence<Node>
        get() = _nodes.map { it.debugNode }

    val edges: Sequence<Pair<Node, Node>>
        get() = sequence {
            _nodes.forEach {
                yieldAll(it.children.map { targetNode -> it.debugNode to targetNode.debugNode })
            }
        }

    val suffixes: Sequence<Pair<Node, Node>>
        get() = _nodes.mapNotNull { sourceNode ->
            sourceNode.takeUnless { it is RootNode }
                    ?.nextSuffix
                    ?.debugNode
                    ?.let { sourceNode.debugNode to it }
        }

    fun add(_sourceNodes: List<Node>) {
        val rootNode = RootNode()
        addTo(_sourceNodes, SuffixPointer(rootNode))
        addChildrenTo(rootNode, SuffixPointer(this.rootNode))
    }

    fun set(_sourceNodes: List<Node>) {
        addTo(_sourceNodes, SuffixPointer(rootNode))
    }

    private fun addTo(_sourceNodes: List<Node>, rootPointer: SuffixPointer) {
        var pointer = rootPointer
        val addedNodes = mutableListOf<Node>()
        for (i in _sourceNodes.indices) {
            var child = pointer.getChildForNode(_sourceNodes, i)
            addedNodes += _sourceNodes[i]
            while (child == null) {
                pointer.createChildOrAcceptNode(addedNodes, addedNodes.lastIndex)
                pointer = pointer.nextSuffixExcept(rootPointer)
                child = pointer.getChildForNode(_sourceNodes, i)
            }
            pointer = child
        }
    }

    private fun addChildrenTo(source: SuffixNode, _target: SuffixPointer, substrLength: Int = 0) {
        var target = _target
        if (target.node !is RootNode) {
            assert(source.from + 1 <= source.run { to ?: nodes.size })
            target.node.addOccurrence(
                    FragmentOccurrence(source.nodes, source.from - substrLength, source.from + 1)
            )

            for (i in source.from + 1 until source.run { (to ?: nodes.size) }) {
                target = target.createChildOrAcceptNode(source.nodes, i)
                target.node.addOccurrence(FragmentOccurrence(source.nodes, source.from - substrLength, i + 1))
            }

            assert(target.node.occurrences.isNotEmpty())
        }

        for (sourceChild in source.children) {
            addChildrenTo(
                    sourceChild,
                    target.createChildOrAcceptNode(sourceChild.nodes, sourceChild.from),
                    substrLength + source.run { (to ?: nodes.size) - from }
            )
        }
    }

    class SuffixPointer(val node: SuffixNode, val prefixLength: Int = 1) {
        fun nextSuffixExcept(except: SuffixPointer): SuffixPointer =
                if (node == except.node) {
                    except
                } else {
                    node.nextSuffix.getChild(
                            1,
                            node.nodes,
                            node.from + 1,
                            node.from + prefixLength
                    )!!
                }

        fun getChildForNode(sourceNodes: List<Node>, i: Int) =
                node.getChild(prefixLength, sourceNodes, i, i + 1)

        fun createChildOrAcceptNode(sourceNodes: List<Node>, i: Int) =
                if (node.nodeAt(prefixLength) similarTo sourceNodes[i]) {
                    SuffixPointer(node, prefixLength + 1)
                } else {
                    getChildForNode(sourceNodes, i)
                            ?: SuffixPointer(node.createChildAt(prefixLength, sourceNodes, i))
                }
    }

    open class SuffixNode(
            val nodes: List<Node>,
            val from: Int,
            var parent: SuffixNode?
    ) {
        var to: Int? = null
        var children = mutableListOf<SuffixNode>()
        var occurrences = mutableMapOf<Node, FragmentOccurrence>()
        val length get() = (to ?: nodes.size) - from
        open val nextSuffix: SuffixNode by lazy {
            parent!!.nextSuffix
                    .getChild(1, parent!!.nodes, parent!!.from + 1, parent!!.to!!)!!
                    .getChildForNode(nodes, from)!!.node
        }

        open val firstNode get() = nodes[from]
        open val debugNode by lazy {
            DebugNode(nodes.subList(from, to ?: nodes.size), occurrences)
        }

        open fun nodeAt(i: Int) =
                if (i < length) nodes[from + i]
                else null

        open fun createChildAt(splitAfter: Int, sourceNodes: List<Node>, i: Int): SuffixNode {
            assert(splitAfter > 0)
            assert(i < sourceNodes.size)
            val child = SuffixNode(sourceNodes, i, this)
            if (from + splitAfter == (to ?: nodes.size)) {
                children.add(child)
            } else {
                val graft = SuffixNode(nodes, from + splitAfter, this).also {
                    it.to = to
                    it.children = children
                    it.occurrences = occurrences
                    children.forEach { child -> child.parent = it }
                    assert(it.length > 0)
                }
                children = mutableListOf(graft, child)
                occurrences = occurrences.mapValues { (_, v) ->
                    FragmentOccurrence(v.nodes, v.from, v.from + splitAfter)
                }.toMutableMap()
            }
            to = from + splitAfter
            return child
        }

        open fun getChild(skipLength: Int, sourceNodes: List<Node>, from: Int, to: Int): SuffixPointer? {
            val pathLength = to - from
            require(pathLength >= 0) { "pathLength=$pathLength but must not be less than 0" }
            require(skipLength <= length) { "skipLength=$skipLength but must be less than current node" }
            val remainedLength = length - skipLength
            for (i in 0 until min(remainedLength, pathLength)) {
                if (!(this.nodes[this.from + skipLength + i] similarTo sourceNodes[from + i])) {
                    return null
                }
            }

            if (skipLength + pathLength <= length) {
                return SuffixPointer(this, skipLength + pathLength)
            }

            return children
                    .singleOrNull { it.firstNode similarTo sourceNodes[from + remainedLength] }
                    ?.getChild(1, sourceNodes, from + remainedLength + 1, to)
        }

        fun addOccurrence(occurrence: FragmentOccurrence) {
            val firstNode = occurrence.nodes[occurrence.from]
            occurrences[firstNode] = occurrence
        }
    }

    class FragmentOccurrence(val nodes: List<Node>, val from: Int, val to: Int) {
        constructor(source: FragmentOccurrence, shift: Int)
                : this(source.nodes, source.from, min(source.nodes.size, source.to + shift))
        val len get() = to - from
    }

    private class RootNode : SuffixNode(emptyList(), 0, null) {
        override val firstNode by lazy {
            object : Node {
                override val id = 0x64252672
                override val filename = "*"
                override val line = 0
                override val column = 0
                override val parent: Nothing? = null
                override val source = Unit
                override val isCommutative = false
                override val code = "*"
                override val children = emptyList<Node>()
                override val inputs get() = throw NotImplementedError()
                override val outputs get() = throw NotImplementedError()
                override val result get() = throw NotImplementedError()
            }
        }

        override val nextSuffix by lazy {
            object : SuffixNode(emptyList(), 0, null) {
                init {
                    to = 0
                }

                override fun getChild(skipLength: Int, sourceNodes: List<Node>, from: Int, to: Int): SuffixPointer? {
                    assert(skipLength == 1)
                    return if (from < to) {
                        SuffixPointer(this@RootNode)
                    } else {
                        SuffixPointer(this)
                    }
                }
            }
        }

        override val debugNode by lazy { DebugNode(listOf(firstNode), occurrences) }

        init {
            to = 0
        }

        override fun createChildAt(splitAfter: Int, sourceNodes: List<Node>, i: Int): SuffixNode {
            assert(splitAfter == 1)
            val child = SuffixNode(sourceNodes, i, this)
            children.add(child)
            return child
        }

        override fun getChild(skipLength: Int, sourceNodes: List<Node>, from: Int, to: Int): SuffixPointer? {
            assert(skipLength == 1)
            if (from == to) return SuffixPointer(this)
            return children
                    .singleOrNull { it.firstNode similarTo sourceNodes[from] }
                    ?.getChild(1, sourceNodes, from + 1, to)
        }
    }

    class DebugNode(
            nodes: List<Node>,
            occurrences: Map<Node, FragmentOccurrence>
    ) : Node by nodes.first() {
        override val id = hashCode()
        override val code =
                occurrences.mapNotNull {
                    it.key.run { "$filename:$line:$column" }
                }.joinToString("\n") + "\n\n" + nodes.joinToString("\n") { it.code }
    }
}

private fun <T> generateBreadthFirstSequence(seed: T, next: (T) -> Iterable<T>): Sequence<T> =
        sequence {
            yield(seed)
            next(seed).forEach { yieldAll(generateBreadthFirstSequence(it, next)) }
        }
