package io.github.mreditor.codesimilarity.plugin.idea.psi

import com.intellij.psi.*
import io.github.mreditor.codesimilarity.plugin.idea.models.Datum

class PsiDatum(source: PsiNamedElement) : Datum(
        33 * source.hashCode(), // to distinguish from corresponding expresssion
        source.name ?: "unknown",
        source.containingFile.name,
        source.line,
        source.column,
        source.text,
        getDefinitionScope(source),
        source
)

private fun getDefinitionScope(source: PsiElement): PsiElement =
        when (source) {
            is PsiVariable -> source.parent
            // TODO remove // is PsiDeclarationStatement -> source.parent
            else -> getDefinitionScope(source.parent)
        }

