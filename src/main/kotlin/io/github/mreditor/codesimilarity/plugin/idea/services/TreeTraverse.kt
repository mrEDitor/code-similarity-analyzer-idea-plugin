package io.github.mreditor.codesimilarity.plugin.idea.services

import io.github.mreditor.codesimilarity.plugin.idea.models.Datum
import io.github.mreditor.codesimilarity.plugin.idea.models.Node

class TreeTraverse {
    fun flatten(node: Node): Sequence<Node> = sequence {
        yield(node)
        //val children = node.children
        val children = if (node.isCommutative) normalSort(node.children)
         else node.children
        for (child in children) {
            yieldAll(flatten(child))
        }
    }

    private fun normalSort(nodes: List<Node>): List<Node> {
        val writersByData = mutableMapOf<Datum, MutableList<Node>>()
        for (node in nodes) {
            for (output in node.outputs) {
                writersByData.getOrPut(output) { mutableListOf() } += node
            }
        }

        val roots = nodes.toMutableSet().apply { add(TopologicalRootNode) }
        val edges = roots.associateWith { mutableListOf<Node>() }.toMutableMap()
        for (node in nodes) {
            edges[TopologicalRootNode]!! += node
            for (datum in node.outputs + node.inputs) {
                writersByData[datum]?.filter { it != node }?.forEach {
                    assert(node.filename == it.filename)
                    if (node.line != it.line) {
                        edges.add(roots, node, it, it.line - node.line)
                    } else {
                        edges.add(roots, node, it, it.column - node.column)
                    }
                }
            }
        }

        // TODO consider external memory sorting for large inputs
        return reverseTopologicalSort(TopologicalRootNode, edges)
                .asIterable()
                .reversed()
                .dropWhile { it === TopologicalRootNode }
                .also { assert(edges.isEmpty()) }
    }

    /**
     * Unstable topological sort by recursive depth-first search.
     * Children traverse order defined by subtree cardinality.
     * Note: consumes visited nodes from {@param edges}.
     */
    private fun reverseTopologicalSort(
            root: Node,
            edges: MutableMap<Node, MutableList<Node>>,
            cardinality: MutableMap<Node, Int> = mutableMapOf()
    ): Sequence<Node> = sequence {
        edges.remove(root)?.run {
            sortedBy { getCardinality(it, cardinality) }.forEach {
                yieldAll(reverseTopologicalSort(it, edges))
            }
            yield(root)
        }
    }

    private fun getCardinality(node: Node, cache: MutableMap<Node, Int>): Int =
        cache.getOrPut(node) {
            1 + node.children.sumBy { getCardinality(it, cache) }
        }

    private fun <T> MutableMap<T, MutableList<T>>.add(
            roots: MutableSet<T>,
            a: T,
            b: T,
            dir: Int
    ) {
        assert(dir != 0)
        if (dir > 0) {
            getOrPut(a) { mutableListOf() } += b
            roots -= b
        } else {
            getOrPut(b) { mutableListOf() } += a
            roots -= a
        }
    }

    private object TopologicalRootNode : Node {
        override val id: Nothing get() = throw NotImplementedError()
        override val filename: Nothing get() = throw NotImplementedError()
        override val line: Nothing get() = throw NotImplementedError()
        override val column: Nothing get() = throw NotImplementedError()
        override val parent: Nothing get() = throw NotImplementedError()
        override val source: Nothing get() = throw NotImplementedError()
        override val isCommutative: Nothing get() = throw NotImplementedError()
        override val code: Nothing get() = throw NotImplementedError()
        override val children: Nothing get() = throw NotImplementedError()
        override val inputs: Nothing get() = throw NotImplementedError()
        override val outputs: Nothing get() = throw NotImplementedError()
        override val result: Nothing get() = throw NotImplementedError()
    }
}