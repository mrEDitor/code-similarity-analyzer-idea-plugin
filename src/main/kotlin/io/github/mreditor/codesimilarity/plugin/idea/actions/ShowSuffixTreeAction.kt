package io.github.mreditor.codesimilarity.plugin.idea.actions

import com.intellij.lang.java.JavaLanguage
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.CommonDataKeys
import com.intellij.psi.PsiMethod
import com.intellij.psi.search.GlobalSearchScope
import com.intellij.psi.search.PsiShortNamesCache
import io.github.mreditor.codesimilarity.plugin.idea.http.NodeCache
import io.github.mreditor.codesimilarity.plugin.idea.http.models.EdgeResponse
import io.github.mreditor.codesimilarity.plugin.idea.http.models.NodeResponse
import io.github.mreditor.codesimilarity.plugin.idea.http.models.Response
import io.github.mreditor.codesimilarity.plugin.idea.psi.PsiNodeFactory
import io.github.mreditor.codesimilarity.plugin.idea.services.TreeTraverse
import io.github.mreditor.codesimilarity.plugin.idea.services.SuffixTree
import io.github.mreditor.codesimilarity.plugin.idea.ui.GraphWebRendererDialog

class ShowSuffixTreeAction : AnAction() {

    override fun update(e: AnActionEvent) {
        val psi = e.getData(CommonDataKeys.PSI_ELEMENT)
        e.presentation.isEnabled = psi is PsiMethod && psi.language == JavaLanguage.INSTANCE
    }

    override fun actionPerformed(e: AnActionEvent) {
        val psiMethod = e.getData(CommonDataKeys.PSI_ELEMENT) as PsiMethod
        val scope = GlobalSearchScope.projectScope(psiMethod.project)
        val nodeCache = NodeCache()
        val normalizer = TreeTraverse()
        val projectMethods = mutableListOf<PsiMethod>()
        PsiShortNamesCache.getInstance(psiMethod.project).run {
            processAllMethodNames({ methodName ->
                projectMethods += getMethodsByName(methodName, scope)
                return@processAllMethodNames true
            }, scope, null)
        }
        val psiNodeFactory = PsiNodeFactory(nodeCache)
        for (psiProjectMethod in projectMethods) {
            psiNodeFactory.createNode(psiProjectMethod)
        }
        GraphWebRendererDialog(
                psiMethod.project,
                "Suffix tree",
                isAvailableForProject = true
        ) { request ->
            if (request.id != 0) return@GraphWebRendererDialog Response()

            val suffixTree = SuffixTree()
            if (isForProject) {
                for (psiProjectMethod in projectMethods) {
                    nodeCache.get(psiProjectMethod.body.hashCode())?.let {
                        suffixTree.add(normalizer.flatten(it).toList())
                    }
                }
            } else {
                nodeCache.get(psiMethod.body.hashCode())?.let {
                    suffixTree.set(normalizer.flatten(it).toList())
                }
            }

            val nodes = suffixTree.nodes.map(NodeResponse.Companion::fromNode)
            val edges = suffixTree.edges.map(EdgeResponse.Companion::fromPair)
            val suffixes =
                    if (isForProject) emptySequence()
                    else suffixTree.suffixes.map(EdgeResponse.Companion::fromReversedPair)
            return@GraphWebRendererDialog Response(
                    nodes = nodes,
                    edges = edges + suffixes
            )
        }
    }
}
