package io.github.mreditor.codesimilarity.plugin.idea.http.models

import kotlinx.serialization.Serializable

@Serializable
data class ArrowResponse(
        val enabled: Boolean = true,
        val type: String = "arrow"
)
