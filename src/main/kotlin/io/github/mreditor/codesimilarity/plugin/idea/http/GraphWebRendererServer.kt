package io.github.mreditor.codesimilarity.plugin.idea.http

import com.sun.net.httpserver.HttpExchange
import com.sun.net.httpserver.HttpServer
import io.github.mreditor.codesimilarity.plugin.idea.http.models.*
import org.jetbrains.io.FileResponses
import java.io.FileNotFoundException
import java.io.InputStream
import java.io.PrintWriter
import java.net.InetSocketAddress

class GraphWebRendererServer(
        private val processRequest: (Request) -> Response
) {
    private var server: HttpServer? = null
    val address: String get() = "http://localhost:${server!!.address.port}"

    fun start() {
        server = startServer()
    }

    fun stop() {
        server?.stop(0)
    }

    private fun startServer(): HttpServer {
        var firstException: Exception? = null
        for (port in 3777..3797) {
            try {
                return HttpServer.create(InetSocketAddress(port), 0).apply {
                    createContext("/", ::contentHandler)
                    executor = null
                    start()
                }
            } catch (e: Exception) {
                firstException = firstException ?: e
            }
        }

        throw InnerException(firstException!!)
    }

    private fun contentHandler(http: HttpExchange) {
        try {
            if (http.requestURI.path.endsWith('/')) {
                http.responseHeaders.add("Location", http.requestURI.path + "index.html")
                http.sendResponseHeaders(301, -1)
            } else {
                val contentType = FileResponses.getContentType(http.requestURI.path)
                val response = when (http.requestMethod) {
                    "GET" -> readFile(http.requestURI.path)
                    "POST" -> readNode(http.requestBody)
                    else -> throw UnsupportedOperationException(http.requestMethod)
                }
                http.responseHeaders.add("Content-Type", contentType)
                http.sendResponseHeaders(200, 0)
                http.responseBody.use {
                    it.write(response)
                }
            }
        } catch (e: Throwable) {
            val code = when (e) {
                is FileNotFoundException -> 404
                else -> 500
            }
            http.responseHeaders.add("Content-Type", "text/plain")
            http.sendResponseHeaders(code, 0)
            http.responseBody.use { responseByte ->
                PrintWriter(responseByte, false, Charsets.UTF_8).use {
                    it.write("HTTP $code\n")
                    e.printStackTrace(it)
                }
            }
        }
    }

    private fun readFile(requestedPath: String): ByteArray {
        val path = "/static$requestedPath"
        return javaClass.getResourceAsStream(path)?.use { it.readBytes() }
                ?: throw FileNotFoundException(path)
    }

    private fun readNode(requestBody: InputStream): ByteArray {
        requestBody.reader(Charsets.UTF_8).use {
            val request = Request.fromJson(it.readText())
            return processRequest(request).toJson().toString().toByteArray()
        }
    }

    private class InnerException(e: Exception) : RuntimeException(e)
}