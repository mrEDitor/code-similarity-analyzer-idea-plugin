package io.github.mreditor.codesimilarity.plugin.idea.http

import io.github.mreditor.codesimilarity.plugin.idea.models.Node

class NodeCache {
    private val map = mutableMapOf<Int, Node>()

    fun add(node: Node) {
        map[node.id] = node
    }

    fun get(id: Int): Node? {
        return map[id]
    }
}