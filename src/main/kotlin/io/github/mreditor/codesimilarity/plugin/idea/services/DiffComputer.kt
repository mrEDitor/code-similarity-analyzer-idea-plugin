package io.github.mreditor.codesimilarity.plugin.idea.services

import com.intellij.diff.comparison.ComparisonPolicy
import com.intellij.diff.contents.DocumentContent
import com.intellij.diff.fragments.LineFragment
import com.intellij.diff.fragments.LineFragmentImpl
import com.intellij.diff.util.DiffUserDataKeysEx
import com.intellij.openapi.application.ReadAction
import com.intellij.openapi.progress.ProgressIndicator
import com.intellij.psi.*
import com.intellij.refactoring.suggested.endOffset
import com.intellij.refactoring.suggested.startOffset
import io.github.mreditor.codesimilarity.plugin.idea.models.Node
import io.github.mreditor.codesimilarity.plugin.idea.psi.document
import io.github.mreditor.codesimilarity.plugin.idea.ui.SearchClonesDialog

class DiffComputer(
        private val leftDocument: Pair<SearchClonesDialog.FileNode, DocumentContent>,
        private val projectMethods: MutableMap<PsiMethod, List<Node>>,
        private val suffixTree: SuffixTree,
        private val selectedValue: SearchClonesDialog.FileNode
) : DiffUserDataKeysEx.DiffComputer {
    override fun compute(text1: CharSequence, text2: CharSequence, policy: ComparisonPolicy, innerChanges: Boolean, indicator: ProgressIndicator): List<LineFragment> =
        ReadAction.compute<List<LineFragment>, Exception> {
            leftDocument.first.psiClass?.methods?.flatMap { psiClassMethod ->
                val result = mutableListOf<Pair<SuffixTree.FragmentOccurrence, SuffixTree.FragmentOccurrence>>()
                val methodNodes = projectMethods[psiClassMethod] ?: return@flatMap listOf<LineFragment>()
                var current = suffixTree.root
                val resultBuffer = mutableMapOf<Node, Pair<SuffixTree.FragmentOccurrence, SuffixTree.FragmentOccurrence>>()
                var u = 0
                for (i in methodNodes.indices) {
                    current = current.getChildForNode(methodNodes, i)!!
                    val leftOccurrences = current.occurrences.fromFile(leftDocument.first.file)
                    val rightOccurrences = current.occurrences.fromFile(selectedValue.file)
                    var anyUpd = false
                    ++u
                    for ((_, left) in leftOccurrences) {
                        for ((k, right) in rightOccurrences) {
                            if (resultBuffer[k]?.let { it.first.endOffset <= left.endOffset } != false) {
                                if (u > SearchClonesDialog.MIN_LENGTH) {
                                    resultBuffer[k] = Pair(left, right)
                                }
                                anyUpd = true
                            }
                        }
                    }
                    if (!anyUpd) {
                        u = 0
                        current = suffixTree.root
                    }
                }
                result += resultBuffer.values
                val resultMap = result.mapNotNull { (left, right) ->
                    try {
                        val len = Integer.min(left.len, right.len)
                        val leftFrom = left.nodes[left.from]
                        val rightFrom = right.nodes[right.from]
                        val leftTo = left.nodes.drop(left.from)
                                .take(len).filterNot(::isMethodBlock).maxBy { it.endOffset }!!
                        val rightTo = right.nodes.drop(right.from)
                                .take(len).filterNot(::isMethodBlock).maxBy { it.endOffset }!!
                        return@mapNotNull LineFragmentImpl(
                                leftFrom.startLine,
                                leftTo.endLine,
                                rightFrom.startLine,
                                rightTo.endLine,
                                leftFrom.startOffset,
                                leftTo.endOffset,
                                rightFrom.startOffset,
                                rightTo.endOffset
                        )
                    } catch (e: Exception) {
                        // todo warning
                        return@mapNotNull null
                    }
                }
                return@flatMap resultMap
            }
        }

    private fun isMethodBlock(it: Node): Boolean {
        return it.source is PsiMethod || it.source is PsiCodeBlock
    }

    private fun isSimilar(left: SuffixTree.FragmentOccurrence, right: SuffixTree.FragmentOccurrence): Boolean {
        if (left.len != right.len) {
            return false
        }
        //TODO use link comparison
        for (i in 0 until right.len) {
            val l = (left.nodes[left.from + i].source as? PsiReferenceExpression)?.resolve() ?: continue
            val r = (right.nodes[right.from + i].source as? PsiReferenceExpression)?.resolve() ?: continue
            if (l !is PsiLocalVariable && r !is PsiLocalVariable && l != r) {
                return false
            }
        }
        return true
    }

    private val Node.startLine: Int
        get() = (source as PsiElement).document.getLineNumber(startOffset) + 1

    private val Node.endLine: Int
        get() = (source as PsiElement).document.getLineNumber(endOffset) + 1

    private val Node.startOffset: Int
        get() = (source as PsiElement).startOffset

    private val Node.endOffset: Int
        get() = (source as PsiElement).endOffset

    private val SuffixTree.FragmentOccurrence.endOffset
        get() = nodes[to - 1].endOffset

    private fun <V> Map<Node, V>.fromFile(file: PsiFile) =
            entries.filter { (it.key.source as PsiElement).containingFile == file }

    private val SuffixTree.SuffixPointer.occurrences: Map<Node, SuffixTree.FragmentOccurrence>
        get() = node.occurrences.mapValues { SuffixTree.FragmentOccurrence(it.value, prefixLength) }
}
