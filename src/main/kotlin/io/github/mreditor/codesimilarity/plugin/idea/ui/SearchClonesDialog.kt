package io.github.mreditor.codesimilarity.plugin.idea.ui

import com.intellij.diff.DiffContentFactory
import com.intellij.diff.DiffManager
import com.intellij.diff.comparison.ComparisonManagerImpl
import com.intellij.diff.contents.DocumentContent
import com.intellij.diff.requests.SimpleDiffRequest
import com.intellij.diff.util.DiffUserDataKeysEx
import com.intellij.openapi.project.Project
import com.intellij.openapi.ui.DialogWrapper
import com.intellij.psi.PsiClass
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiFile
import com.intellij.psi.PsiMethod
import com.intellij.psi.search.GlobalSearchScope
import com.intellij.psi.search.PsiShortNamesCache
import com.intellij.ui.components.JBList
import com.intellij.ui.components.JBScrollPane
import com.intellij.ui.treeStructure.Tree
import io.github.mreditor.codesimilarity.plugin.idea.http.NodeCache
import io.github.mreditor.codesimilarity.plugin.idea.models.Node
import io.github.mreditor.codesimilarity.plugin.idea.psi.PsiNodeFactory
import io.github.mreditor.codesimilarity.plugin.idea.psi.document
import io.github.mreditor.codesimilarity.plugin.idea.services.DiffComputer
import io.github.mreditor.codesimilarity.plugin.idea.services.SuffixTree
import io.github.mreditor.codesimilarity.plugin.idea.services.TreeTraverse
import java.awt.BorderLayout
import java.awt.Dimension
import javax.swing.DefaultListModel
import javax.swing.JComponent
import javax.swing.JPanel
import javax.swing.tree.DefaultMutableTreeNode

class SearchClonesDialog(private val project: Project) : DialogWrapper(project) {
    companion object {
        const val MIN_LENGTH = 15
    }

    private var leftDocument: Pair<FileNode, DocumentContent>? = null
    private val scope = GlobalSearchScope.projectScope(project)
    private val namesCache = PsiShortNamesCache.getInstance(project)
    private val classStructure = DefaultMutableTreeNode(project.name).apply {
        namesCache.processAllClassNames({ shortClassName ->
            for (psiClass in namesCache.getClassesByName(shortClassName, scope)) {
                var pointer = this
                val classTreeNode = FileNode(
                        psiClass.name ?: "<anonymous class>",
                        psiClass.containingFile,
                        psiClass
                )
                psiClass.qualifiedName?.split('.')?.dropLast(1)?.forEach { namePart ->
                    pointer = pointer.child { it.userObject == namePart }
                            ?: DefaultMutableTreeNode(namePart).also(pointer::add)
                }
                pointer.child { it.userObject == psiClass.name }
                        ?.let { it.userObject = classTreeNode }
                        ?: pointer.add(DefaultMutableTreeNode(classTreeNode))
            }
            return@processAllClassNames true
        }, scope, null)
    }

    init {
        title = "Search and compare clones"
        init()
        show()
    }

    override fun createCenterPanel(): JComponent {
        val nodeCache = NodeCache()
        val normalizer = TreeTraverse()
        val suffixTree = SuffixTree()
        val factory = DiffContentFactory.getInstance()
        val manager = DiffManager.getInstance()
        val panel = manager.createRequestPanel(project, disposable, null)
        val projectMethods = mutableMapOf<PsiMethod, List<Node>>()
        val psiNodeFactory = PsiNodeFactory(nodeCache)
        val comparisonManager = ComparisonManagerImpl.getInstanceImpl()
        PsiShortNamesCache.getInstance(project).run {
            processAllMethodNames({ methodName ->
                projectMethods += getMethodsByName(methodName, scope)
                        .filter { it.body != null }
                        .associateWith { normalizer.flatten(psiNodeFactory.createNode(it.body!!)).toList() }
                return@processAllMethodNames true
            }, scope, null)
        }
        for (psiProjectMethod in projectMethods) {
            suffixTree.add(psiProjectMethod.value)
        }
        return JPanel(BorderLayout()).apply {
            val rightPaneModel = DefaultListModel<FileNode>()
            val rightPane = JBList(rightPaneModel).apply {
                addListSelectionListener {
                    if (selectedValue == null) {
                        panel.setRequest(null)
                        return@addListSelectionListener
                    }
                    leftDocument?.let { leftDocument ->
                        val rightDocument = factory.create(project, selectedValue.file.document)
                        panel.setRequest(SimpleDiffRequest(
                                null,
                                leftDocument.second,
                                rightDocument,
                                leftDocument.first.file.name,
                                selectedValue.file.name
                        ).apply {
                            putUserData(
                                    DiffUserDataKeysEx.CUSTOM_DIFF_COMPUTER,
                                    DiffComputer(leftDocument, projectMethods, suffixTree, selectedValue)
                            )
                        })
                    }
                }
            }
            val leftPane = Tree(classStructure).apply {
                addTreeSelectionListener {
                    val node = getSelectedNodes(DefaultMutableTreeNode::class.java) { true }
                            .singleOrNull()
                            ?.userObject as? FileNode
                    node?.let {
                        leftDocument = Pair(node, factory.create(project, node.file.document))
                        val matchingFiles = HashSet<PsiFile>()
                        rightPaneModel.clear()
                        node.psiClass?.methods?.forEach { psiClassMethod ->
                            val methodNodes = projectMethods[psiClassMethod] ?: return@forEach
                            var current = suffixTree.root
                            for (i in methodNodes.indices) {
                                current = current.getChildForNode(methodNodes, i)!!
                                if (i > MIN_LENGTH) {
                                    matchingFiles += current.node.occurrences
                                            .flatMap { it.value.nodes }
                                            .map { (it.source as PsiElement).containingFile }
                                }
                            }
                        }
                        for (matchingFile in matchingFiles) {
                            rightPaneModel.addElement(
                                    FileNode(matchingFile.name, matchingFile, null)
                            )
                        }
                    }
                }
            }
            add(
                    JBScrollPane(leftPane).apply { preferredSize = Dimension(160, 160) },
                    BorderLayout.WEST
            )
            add(
                    JBScrollPane(rightPane).apply { preferredSize = Dimension(160, 160) },
                    BorderLayout.EAST
            )
            add(panel.component, BorderLayout.CENTER)
        }
    }

    class FileNode(val name: String, val file: PsiFile, val psiClass: PsiClass?) {
        override fun toString() = name
    }

    private fun DefaultMutableTreeNode.child(predicate: (DefaultMutableTreeNode) -> Boolean) =
            this.children().asSequence().filterIsInstance<DefaultMutableTreeNode>().singleOrNull(predicate)
}

