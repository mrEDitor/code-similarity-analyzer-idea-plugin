package io.github.mreditor.codesimilarity.plugin.idea.http.models

import io.github.mreditor.codesimilarity.plugin.idea.models.Datum
import kotlinx.serialization.Serializable

@Serializable
data class DatumResponse(
        val id: Int,
        val position: Int,
        val label: String,
        val title: String
) {
    val color = "#FFDA84"

    companion object Factory {
        fun fromDatum(datum: Datum) = DatumResponse(
                datum.id,
                (datum.line shl 20) or datum.column,
                datum.code,
                "${datum.filename}:$datum.line}:${datum.column}<br>${datum.source.javaClass}"
        )
    }
}
