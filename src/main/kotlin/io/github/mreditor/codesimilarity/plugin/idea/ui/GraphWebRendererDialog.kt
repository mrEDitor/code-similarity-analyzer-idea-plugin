package io.github.mreditor.codesimilarity.plugin.idea.ui

import com.intellij.ide.browsers.BrowserLauncherAppless
import com.intellij.openapi.project.Project
import com.intellij.openapi.ui.DialogWrapper
import com.intellij.ui.components.JBLabel
import io.github.mreditor.codesimilarity.plugin.idea.http.GraphWebRendererServer
import io.github.mreditor.codesimilarity.plugin.idea.http.models.Request
import io.github.mreditor.codesimilarity.plugin.idea.http.models.Response
import java.awt.event.ActionEvent
import javax.swing.BoxLayout
import javax.swing.JCheckBox
import javax.swing.JPanel


class GraphWebRendererDialog(
        project: Project,
        graphType: String,
        isAvailableForProject: Boolean = false,
        processRequest: GraphWebRendererDialog.(Request) -> Response
) : DialogWrapper(project) {
    private val server = GraphWebRendererServer { processRequest(it) }
    private val centerPanel = JPanel().also {
        it.layout = BoxLayout(it, BoxLayout.Y_AXIS)
    }
    private val projectCheckbox = JCheckBox("Whole project")
    private val browseAction = object : DialogWrapperAction("Open browser") {
        override fun doAction(e: ActionEvent) {
            BrowserLauncherAppless().open(server.address)
        }
    }

    val isForProject get() = projectCheckbox.isSelected

    init {
        try {
            server.start()
            centerPanel.run {
                add(JBLabel("Use web browser to access $graphType at ${server.address}"))
                if (isAvailableForProject) {
                    add(projectCheckbox)
                }
            }
        } catch (e: InnerException) {
            centerPanel.add(JBLabel("Unable to start Web UI: ${e.message}"))
            browseAction.isEnabled = false
        }

        title = "$graphType renderer"
        init()
        show()
    }

    override fun createActions() = arrayOf(browseAction, cancelAction)

    override fun createCenterPanel() = centerPanel

    override fun dispose() {
        server.stop()
        super.dispose()
    }

    private class InnerException(e: Exception) : RuntimeException(e)
}