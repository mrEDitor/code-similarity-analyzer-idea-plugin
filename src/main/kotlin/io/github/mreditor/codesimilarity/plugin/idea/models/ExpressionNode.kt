package io.github.mreditor.codesimilarity.plugin.idea.models

class ExpressionNode(
        override val code: String,
        override val filename: String,
        override val line: Int,
        override val column: Int,
        override val parent: Node?,
        override val source: Any,
        override val isCommutative: Boolean
) : Node {
    override val id: Int = source.hashCode()
    override val children = mutableListOf<Node>()
    override val inputs = DoubleLinkedSet(this, Datum::inputs)
    override val outputs = DoubleLinkedSet(this, Datum::outputs)
    override val result = mutableSetOf<Datum>()

    override fun toString() =
        "ExpressionNode($filename:$line:$column): $code"
}
