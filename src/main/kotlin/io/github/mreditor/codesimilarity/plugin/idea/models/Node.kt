package io.github.mreditor.codesimilarity.plugin.idea.models

interface Node {
    val id: Int
    val filename: String
    val line: Int
    val column: Int
    val parent: Node?
    val source: Any
    val isCommutative: Boolean
    val code: String
    val children: List<Node>
    val inputs: DoubleLinkedSet<Datum, Node>
    val outputs: DoubleLinkedSet<Datum, Node>
    val result: Set<Datum>
}

infix fun Node?.similarTo(node: Node) =
        this?.similarityCode == node.similarityCode

val Node.similarityCode: Int
    get() = source.javaClass.hashCode()
