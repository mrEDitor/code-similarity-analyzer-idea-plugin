window.onload = function () {
    const nodes = new vis.DataSet();
    const edges = new vis.DataSet();
    const network = new vis.Network(
        document.getElementById('g'),
        {nodes: nodes, edges: edges},
        {
            // todo something with glitches
            layout: {
                hierarchical: {
                    enabled: true,
                    direction: 'LR',
                    sortMethod: 'directed',
                }
            },
            physics: {
                enabled: false
            }
        }
    );
    network.on('click', props => props.nodes.forEach(addNode));
    network.layoutEngine.direction.sort = function (nodeArray) {
        return nodeArray.sort((a, b) => a.options.position - b.options.position);
    };
    addNode(0);

    async function addNode(id) {
        console.log("loading #" + id);
        const response = await fetch(document.location, {
            method: 'POST',
            body: JSON.stringify({id: id}),
            headers: {Accepts: "application/json"},
        });
        if (response.ok) {
            const json = await response.json();
            console.log("adding", json);
            nodes.update(json.nodes);
            nodes.update(json.data);
            edges.update(json.edges);
        } else {
            console.log("error", await response.text());
            alert("unable to fetch node #" + id);
        }
    }
};
